#!/usr/bin/env python

#  Copyright (c) <2021> <Mikkel Heinrich, Aleksander Nikolajsen, Tobias Frejo Rasmussen>

#  Permission is hereby granted, free of charge, to any person 
#  obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use,
#  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy
import numpy as np

CENTER  = 0
LEFT    = 1
RIGHT   = 2
R_LEFT  = 3
R_RIGHT = 4

GET_DIRECTION = 0
DRIVE_FORWARD = 1
RIGHT_TURN    = 2
LEFT_TURN     = 3

LINEAR_VELOCITY = 0.3
ANGULAR_VELOCITY = 1.0

class WalleDrive():
    walle_state_num = 0

    cmd_vel_pub_:rospy.Publisher = None
    laser_scan_sub_:rospy.Subscriber = None
    odom_sub_:rospy.Subscriber = None

    escape_range_ = 0.0
    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0
    dist_tolerance = 0.0

    scan_data_ = [0.0, 0.0, 0.0, 0.0, 0.0]
    big_scan_data_ = [0.0] * 91

    walle_pose_ = 0.0
    prev_walle_pose_ = 0.0

    is_stuck = False

    is_colided = False
    collisions = 0
    speed_updates = 0
    speed_accumulated = 0.0
    speed_average = 0.0

    current_lin_vel = 0
    current_pos = (0, 0)

    def __init__(self):

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        # initialize variables
        self.escape_range_ = 30.0 * (np.pi / 180.0)
        self.check_forward_dist_ = 0.52
        self.check_side_dist_ = 0.52
        self.dist_tolerance = 0.1

        self.walle_pose_ = 0.0
        self.prev_walle_pose_ = 0.0

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallBack, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)

    def __del__(self):
         self.updatecommandVelocity(0.0, 0.0)

    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)

        self.walle_pose_ = np.arctan2(siny, cosy)
        self.current_pos = (data.pose.pose.position.x, data.pose.pose.position.y)

    #Utilizing the laserscanner
    def laserScanMsgCallBack(self, data:LaserScan):
        scan_angle = [0, 30, 330, 90, 270]

        for i in range(len(scan_angle)):
            if np.isinf(data.ranges[i]):
                self.scan_data_[i] = data.range_max
            else:
                self.scan_data_[i] = data.ranges[scan_angle[i]]
        
        for i in range(91):
            # map [0;180] to [-90;90]
            angle = i - 45
            if np.isinf(data.ranges[angle]):
                self.big_scan_data_[i] = data.range_max
            else:
                self.big_scan_data_[i] = data.ranges[angle]

        if self.scan_data_[CENTER] < 0.1 or min(self.scan_data_[1:]) < 0.14:
            if not self.is_colided:
                rospy.logwarn("Wall-E has collided")
                self.collisions += 1
                self.is_colided = True
        elif self.is_colided:
            self.is_colided = False
            rospy.logwarn("Wall-E is free!")

    #  For updating both the linear and the angular velocity
    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        self.speed_accumulated += lin_vel
        self.speed_updates += 1
        self.speed_average = self.speed_average + (lin_vel-self.speed_average)/self.speed_updates   # Won't overflow

        self.current_lin_vel = lin_vel

        # rospy.loginfo(cmd_vel)
        self.cmd_vel_pub_.publish(cmd_vel)

    def turn(self, pct: float, direction = 1):
        lin_pct = 1.0 - np.abs(pct)
        self.updatecommandVelocity(LINEAR_VELOCITY * direction * lin_pct, ANGULAR_VELOCITY * pct)

    #  Looping the different movement functions and defining when they are used
    def controlLoop(self):
        rate = rospy.Rate(125) # 125 Hz

        if self.walle_state_num == GET_DIRECTION:
            if self.scan_data_[CENTER] > self.check_forward_dist_:
                if self.scan_data_[LEFT] < self.check_side_dist_:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = RIGHT_TURN

                elif self.scan_data_[RIGHT] < self.check_side_dist_:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = LEFT_TURN

                else:
                    self.walle_state_num = DRIVE_FORWARD

            if self.scan_data_[CENTER] < self.check_forward_dist_:
                self.prev_walle_pose_ = self.walle_pose_
                if self.scan_data_[LEFT] < self.scan_data_[RIGHT]:
                    self.walle_state_num = RIGHT_TURN
                else:
                    self.walle_state_num = LEFT_TURN

        elif self.walle_state_num == DRIVE_FORWARD:
            self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.walle_state_num = GET_DIRECTION
        
        elif self.walle_state_num == RIGHT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.turn(-0.5)
                #self.updatecommandVelocity(0.0, -1 * ANGULAR_VELOCITY)

        elif self.walle_state_num == LEFT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.turn(0.5)
                #self.updatecommandVelocity(0.0, ANGULAR_VELOCITY)

        else:
            self.walle_state_num = GET_DIRECTION

        rate.sleep()
    
    def softTurnControlLoop(self):
        rate = rospy.Rate(125) # 125 Hz

        biggest_angle = self.big_scan_data_.index(max(self.big_scan_data_)) # [-90;90]
        turn_pct = (biggest_angle-45)/45 # [-1.0; 1.0]

        if self.walle_state_num == GET_DIRECTION:
            # rospy.loginfo(f"scan_data: {self.scan_data_}")
            if self.scan_data_[CENTER] > self.check_forward_dist_:
                if self.scan_data_[LEFT] < self.check_side_dist_:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = RIGHT_TURN

                elif self.scan_data_[RIGHT] < self.check_side_dist_:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = LEFT_TURN

                else:
                    self.walle_state_num = DRIVE_FORWARD

            if self.scan_data_[CENTER] < self.check_forward_dist_:
                self.prev_walle_pose_ = self.walle_pose_
                if self.scan_data_[LEFT] < self.scan_data_[RIGHT]:
                    self.walle_state_num = RIGHT_TURN
                else:
                    self.walle_state_num = LEFT_TURN

        elif self.walle_state_num == DRIVE_FORWARD:
            #self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.turn(turn_pct)
            self.walle_state_num = GET_DIRECTION
        
        elif self.walle_state_num == RIGHT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.turn(-.5)
        
        elif self.walle_state_num == LEFT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.turn(.5)

        else:
            self.walle_state_num = GET_DIRECTION

        rate.sleep()

    def FollowRight(self):
        rate = rospy.Rate(30) # 125 Hz

        dist = 0.2
        if self.walle_state_num == GET_DIRECTION:
            if self.scan_data_[CENTER] > dist:
                #Continue forward
                if np.abs(self.scan_data_[R_RIGHT] - dist) < self.dist_tolerance:
                    self.walle_state_num = DRIVE_FORWARD
                elif self.scan_data_[R_RIGHT] < dist:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = LEFT_TURN
                else:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = RIGHT_TURN
            else:
                #Enter corner
                self.prev_walle_pose_ = self.walle_pose_
                self.walle_state_num = LEFT_TURN

        elif self.walle_state_num == DRIVE_FORWARD:
            self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.walle_state_num = GET_DIRECTION
        
        elif self.walle_state_num == RIGHT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.updatecommandVelocity(0.0, -0.2 * ANGULAR_VELOCITY)

        elif self.walle_state_num == LEFT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.updatecommandVelocity(0.0, 0.2 * ANGULAR_VELOCITY)

        else:
            self.walle_state_num = GET_DIRECTION
        rate.sleep()

if __name__ == "__main__":
    rospy.init_node("walle_pydrive")

    bot = WalleDrive()

    while not rospy.is_shutdown():
        bot.softTurnControlLoop()
        #bot.controlLoop()
        #bot.FollowRight()

        data = {
            'current_lin_vel': bot.current_lin_vel,
            'lin_avg': bot.speed_average,
            'collisions': bot.collisions,
            'pos' : {
                'x': bot.current_pos[0],
                'y': bot.current_pos[1]
            }
        }
        rospy.loginfo(data)

    bot.updatecommandVelocity(0.0, 0.0)
