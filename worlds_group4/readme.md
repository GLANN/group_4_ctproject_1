# Group 4 worlds

The labyrinths made by group 4.

## Install
If you have a default installation by following the ROS tutorial, place the folder `worlds_group16` inside `~/catkin_ws/src`. Afterwards run
```
cd ~/catkin_ws
catkin_make
source devel/setup.bash
```

## Run

Run our maze by excecuting the following command:

```
roslaunch worlds_group4 world_group4.launch
```

In a new terminal, run your navigation code.

### Other mazes
Instead of the launch command above, you can try 4 other of our mazes with
```
roslaunch worlds_group4 mazeX.launch
```
replacing the X with a number from 0 to 3. An image of each of the mazes is found below:

## Mazes
### Main maze (world_group4)
![Image of the main maze](world_group4.png)

Get to the flag as fast as possible!

### Maze 0
![Image of maze 0](maze0.png)

Try some round corners

### Maze 1
![Image of maze 1](maze1.png)

Get to the green square

### Maze 2
![Image of maze 2](maze2.png)

An ugly brown thing

### Maze 3
![Image of maze 3](maze3.png)

A designer's nightmare
