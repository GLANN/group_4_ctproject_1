#!/usr/bin/python
from __future__ import print_function

from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy
import numpy as np
import argparse

CENTER = 0
LEFT   = 1
RIGHT  = 2

LINEAR_VELOCITY  = 0.3
ANGULAR_VELOCITY = 1.5

GET_TB3_DIRECTION = 0
TB3_DRIVE_FORWARD = 1
TB3_RIGHT_TURN    = 2
TB3_LEFT_TURN     = 3

class Turtlebot3Drive:

    cmd_vel_pub_:rospy.Publisher = None
    laser_scan_sub_:rospy.Subscriber = None
    odom_sub_:rospy.Subscriber = None

    escape_range_ = 0.0
    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0

    scan_data_ = [0.0, 0.0, 0.0]

    tb3_pose_ = 0.0
    prev_tb3_pose_ = 0.0

    state_num = 0

    def __init__(self, esc_range = 30.0, fwd_dist = 0.4, side_dist = 0.35):

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        rospy.loginfo(f"Initializing Turtlebot3")
        rospy.loginfo(f"Linear velocity : {LINEAR_VELOCITY}")
        rospy.loginfo(f"Angular velocity: {ANGULAR_VELOCITY}")
        rospy.loginfo(f"Escape range: {esc_range}")
        rospy.loginfo(f"Forward dist: {fwd_dist}")
        rospy.loginfo(f"Side dist   : {side_dist}")

        # initialize variables
        self.escape_range_ = esc_range * (np.pi / 180.0)
        self.check_forward_dist_ = fwd_dist
        self.check_side_dist_ = side_dist

        self.tb3_pose_ = 0.0
        self.prev_tb3_pose_ = 0.0

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallBack, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)
    
    def __del__(self):
         #self.updatecommandVelocity(0.0, 0.0)
         #rospy.signal_shutdown("Shutting down")
         pass
    
    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)
        
        self.tb3_pose_ = np.arctan2(siny, cosy)

    def laserScanMsgCallBack(self, data:LaserScan):
        scan_angle = [0, 30, 330]

        for i in range(3):
            if np.isinf(data.ranges[i]):
                self.scan_data_[i] = data.range_max
            else:
                self.scan_data_[i] = data.ranges[scan_angle[i]]

    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        #rospy.loginfo(cmd_vel)
        self.cmd_vel_pub_.publish(cmd_vel)

    def controlLoop(self):
        #print(self.state_num)
        if self.state_num == GET_TB3_DIRECTION:
            #print(f"Scan: {self.scan_data_}, Fwddist: {self.check_forward_dist_}")
            if self.scan_data_[CENTER] > self.check_forward_dist_:

                if self.scan_data_[LEFT] < self.check_side_dist_:
                    self.prev_tb3_pose_ = self.tb3_pose_
                    self.state_num = TB3_RIGHT_TURN

                elif self.scan_data_[RIGHT] < self.check_side_dist_:
                    self.prev_tb3_pose_ = self.tb3_pose_
                    self.state_num = TB3_LEFT_TURN

                else:
                    self.state_num = TB3_DRIVE_FORWARD
            
            if self.scan_data_[CENTER] < self.check_forward_dist_:
                self.prev_tb3_pose_ = self.tb3_pose_
                if self.scan_data_[LEFT] < self.scan_data_[RIGHT]:
                    self.state_num = TB3_RIGHT_TURN
                else:
                    self.state_num = TB3_LEFT_TURN
        
        elif self.state_num == TB3_DRIVE_FORWARD:
            self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.state_num = GET_TB3_DIRECTION

        elif self.state_num == TB3_RIGHT_TURN:
            if np.abs(self.prev_tb3_pose_ - self.tb3_pose_) >= self.escape_range_:
                self.state_num = GET_TB3_DIRECTION
            else:
                self.updatecommandVelocity(0.0, -1 * ANGULAR_VELOCITY)
            
        elif self.state_num == TB3_LEFT_TURN:
            if np.abs(self.prev_tb3_pose_ - self.tb3_pose_) >= self.escape_range_:
                self.state_num = GET_TB3_DIRECTION
            else:
                self.updatecommandVelocity(0.0, ANGULAR_VELOCITY)
            
        else:
            self.state_num = GET_TB3_DIRECTION


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Automatic drive of Turtlebot3')
    parser.add_argument('-L', '--linear_velocity',
                type=float, default=LINEAR_VELOCITY, action="store",
                help='Set linear velocity',
                dest='lin_vel')
    parser.add_argument('-A', '--angular_velocity',
                type=float, default=ANGULAR_VELOCITY, action="store",
                help='Set angular velocity',
                dest='ang_vel')
    parser.add_argument('-e', '--escape_range', 
                type=float, default=30.0, action="store",
                help='Escape Range',
                dest='escape_range')
    parser.add_argument('-f', '--forward_dist',
                type=float, default=0.4, action="store",
                help='Collision Forward Distance',
                dest='forward_dist')
    parser.add_argument('-s', '--side_dist',
                type=float, default=0.35, action="store",
                help='Collision Side Distance',
                dest='side_dist')
    args = parser.parse_args()
    LINEAR_VELOCITY = args.lin_vel
    ANGULAR_VELOCITY = args.ang_vel

    rospy.init_node("turtlebot3_pydrive")

    bot = Turtlebot3Drive(esc_range = args.escape_range, 
                          fwd_dist  = args.forward_dist, 
                          side_dist = args.side_dist)

    while not rospy.is_shutdown():
        try:
            rate = rospy.Rate(500)
            bot.controlLoop()
            rate.sleep()
            #rospy.spin()
        except:
            bot.updatecommandVelocity(0.0, 0.0)
