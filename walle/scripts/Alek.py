#!/usr/bin/env python

#  Copyright (c) <2021> <Mikkel Heinrich, Aleksander Nikolajsen, Tobias Frejo Rasmussen>

#  Permission is hereby granted, free of charge, to any person 
#  obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use,
#  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy
import numpy as np

CENTER  = 0
LEFT    = 1
RIGHT   = 2
R_LEFT  = 3
R_RIGHT = 4

GET_DIRECTION = 0
DRIVE_FORWARD = 1
RIGHT_TURN    = 2
LEFT_TURN     = 3

LINEAR_VELOCITY = 0.3
ANGULAR_VELOCITY = 1.0

class WalleDrive():
    walle_state_num = 0

    cmd_vel_pub_:rospy.Publisher = None
    laser_scan_sub_:rospy.Subscriber = None
    odom_sub_:rospy.Subscriber = None

    escape_range_ = 0.0
    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0
    dist_tolerance = 0.0

    scan_data_ = [0.0, 0.0, 0.0, 0.0, 0.0]

    walle_pose_ = 0.0
    prev_walle_pose_ = 0.0

    def __init__(self):

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        # initialize variables
        self.escape_range_ = 30.0 * (np.pi / 180.0)
        self.check_forward_dist_ = 0.5
        self.check_side_dist_ = 0.5
        self.dist_tolerance = 0.1

        self.walle_pose_ = 0.0
        self.prev_walle_pose_ = 0.0

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallBack, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)

    def __del__(self):
         self.updatecommandVelocity(0.0, 0.0)

    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)

        self.walle_pose_ = np.arctan2(siny, cosy)

    #Utilizing the laserscanner
    def laserScanMsgCallBack(self, data:LaserScan):
        scan_angle = [0, 30, 330, 90, 270]

        for i in range(len(scan_angle)):
            if np.isinf(data.ranges[i]):
                self.scan_data_[i] = data.range_max
            else:
                self.scan_data_[i] = data.ranges[scan_angle[i]]

    #  For updating both the linear and the angular velocity
    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        rospy.loginfo(cmd_vel)
        self.cmd_vel_pub_.publish(cmd_vel)

    def turn(self, pct: float, direction = 1):
        lin_pct = 1.0 - np.abs(pct)
        self.updatecommandVelocity(LINEAR_VELOCITY * direction * lin_pct, ANGULAR_VELOCITY * pct)

    #  Looping the different movement functions and defining when they are used
    def controlLoop(self):
        rate = rospy.Rate(125) # 125 Hz

        # Collision detection.
        if self.walle_state_num == GET_DIRECTION:                       # if the direction is not known, then check data.
            ## check side collision.
            if self.scan_data_[CENTER] > self.check_forward_dist_:      ## if the distance in front of Wall-E is greater than check_forward then,
                if self.scan_data_[LEFT] < self.check_side_dist_:       ### if the distance on the left is less than check_side, then trun right.
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = RIGHT_TURN

                elif self.scan_data_[RIGHT] < self.check_side_dist_:    ### else if the distance on the right is less than check_side, then trun left
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = LEFT_TURN

                else:                                                   ### else drive forward.
                    self.walle_state_num = DRIVE_FORWARD
            ## Check front collision
            if self.scan_data_[CENTER] < self.check_forward_dist_:      ## if the distance in front of Wall-E is less than check_forward then,
                self.prev_walle_pose_ = self.walle_pose_
                if self.scan_data_[LEFT] < self.scan_data_[RIGHT]:      ### if there is less distance to a wall on the left-hand side then turn right.
                    self.walle_state_num = RIGHT_TURN
                else:                                                   ### else turn left.
                    self.walle_state_num = LEFT_TURN

        # Drive forward.
        elif self.walle_state_num == DRIVE_FORWARD:
            self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.walle_state_num = GET_DIRECTION
        
        # Turn Right.
        elif self.walle_state_num == RIGHT_TURN:                                        
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:  ## if the absolute value of Wall-E's position is greater than (or equal to) escape_range then direction is unknown? 
                self.walle_state_num = GET_DIRECTION
            else:                                                                       ## else dynamically turn 50 deg, right 
                self.turn(-0.5)
                #self.updatecommandVelocity(0.0, -1 * ANGULAR_VELOCITY)

        # Turn Left
        elif self.walle_state_num == LEFT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:  ## if the absolute value of Wall-E's position is greater than (or equal to) escape_range then direction is unknown? 
                self.walle_state_num = GET_DIRECTION
            else:                                                                       ## else dynamically turn 50 deg, left 
                self.turn(0.5)
                #self.updatecommandVelocity(0.0, ANGULAR_VELOCITY)

        else:                                                                           # 
            self.walle_state_num = GET_DIRECTION

        rate.sleep()

    def FollowRight(self):
        rate = rospy.Rate(30) # 125 Hz

        dist = 0.2
        if self.walle_state_num == GET_DIRECTION:
            if self.scan_data_[CENTER] > dist:
                #Continue forward
                if np.abs(self.scan_data_[R_RIGHT] - dist) < self.dist_tolerance:
                    self.walle_state_num = DRIVE_FORWARD
                elif self.scan_data_[R_RIGHT] < dist:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = LEFT_TURN
                else:
                    self.prev_walle_pose_ = self.walle_pose_
                    self.walle_state_num = RIGHT_TURN
            else:
                #Enter corner
                self.prev_walle_pose_ = self.walle_pose_
                self.walle_state_num = LEFT_TURN

        elif self.walle_state_num == DRIVE_FORWARD:
            self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.walle_state_num = GET_DIRECTION
        
        elif self.walle_state_num == RIGHT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.updatecommandVelocity(0.0, -0.2 * ANGULAR_VELOCITY)

        elif self.walle_state_num == LEFT_TURN:
            if np.abs(self.prev_walle_pose_ - self.walle_pose_) >= self.escape_range_:
                self.walle_state_num = GET_DIRECTION
            else:
                self.updatecommandVelocity(0.0, 0.2 * ANGULAR_VELOCITY)

        else:
            self.walle_state_num = GET_DIRECTION
        rate.sleep()

if __name__ == "__main__":
    rospy.init_node("walle_pydrive")

    bot = WalleDrive()

    while not rospy.is_shutdown():
        bot.controlLoop()
        #bot.FollowRight()

    bot.updatecommandVelocity(0.0, 0.0)



