# Wall-E - a simulated turtlebot 3

## Table of contents

[[_TOC_]]

## Introduction
The purpose of this project has been for us to learn how to utilize our knowledge of programming gained over the course of our last semester to create a navigation algorithm for a robot that enables it to navigate freely around a random labyrinth from a starting point to an ending point. Thus, this project has acted as a test of how well we are able to apply our technical knowledge in a practical environment.

## Requirements
- ROS (Noetic)
- Gazebo
- Catkin
- The following ROS packages
  - rospy
  - std_msgs
  - geometry_msgs
  - turtlebot3_simulations
  - turtlebot3-msgs
  - turtlebot3

## Installation

1. Follow the [ROS installation guide](http://wiki.ros.org/ROS/Installation) to install the full ROS package.
2. Install [Gazebo](http://gazebosim.org/tutorials?cat=install)
3. Install Turtlebot 3 simulation packages as instructed [here](https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup) and [here](https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/#gazebo-simulation). Be sure to select the correct ROS version at the top of the page.
4. Clone this repository into the `src/` folder of your catkin workspace.
5. Run `catkin_make` from your catkin workspace.

### Installation example on Ubuntu 20.04
To install ROS Noetic and the following programs from the Ubuntu terminal, run the following commands.

Install ROS
```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-noetic-desktop-full
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
```

Install Gazebo
```bash
curl -sSL http://get.gazebosim.org | sh
```

Install Turtlebot3 simulation packages
```bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src/
git clone -b noetic-devel https://github.com/ROBOTIS-GIT/DynamixelSDK.git
git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3.git
```

Install the Wall-E package
```bash
cd ~/catkin_ws/src/
git clone https://gitlab.com/GLANN/group_4_ctproject_1.git
```

Build the packages
```bash
cd ~/catkin_ws && catkin_make
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
```

## Configuration
Make sure the following lines are added  the bottom of your `.bashrc`:
```bash
source /opt/ros/noetic/setup.bash

export OWN_IP=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | sed '2,$d') 
export ROS_HOSTNAME=${OWN_IP}
export ROS_MASTER_URI=http://${OWN_IP}:11311

export IP_OF_TURTLEBOT=192.168.104.135
alias setTurtlebot='export ROS_MASTER_URI=http://${IP_OF_TURTLEBOT}:11311'

export TURTLEBOT3_MODEL=burger
source ~/catkin_ws/devel/setup.bash
```

## Running the simulation
### Open a labyrinth
Start the maze by opening a terminal and call
```
roslaunch worlds_group4 world_group4.launch
```

**(Optional) Other mazes**  
To open our other mazes, [follow the instructions here](worlds_group4/readme.md). Mazes from the other groups are saved in the `group_worlds` package. Open them with 
```
roslaunch group_worlds world_groupX.launch
```
replacing X with the group number.

In case you are interested in trying out a random labyrinth, [follow the instructions here](https://gitlab.com/frejo/pymaze/) to make a random labyrinth of your own!

### Run the self driving script
In a new terminal, run the driver script
```bash
rosrun walle walle_drive.py
```

### (Optional) Observe the laser scan data in RViZ
```
roslaunch turtlebot3_gazebo turtlebot3_gazebo_rviz.launch
```

## Troubleshooting and FAQ
Occasionally, we experienced problems with the laser scanner seeing through walls if the Turtlebot is too close. If this happens, it can be fixed by changing the minimum range (at line 118) in the xacro file located at: 
```
~/catkin_ws/src/turtlebot3/turtlebot3_description/urdf/turtlebot3_burger.gazebo.xacro
```
Then lower the min range to minimum 0.08 instead of 0.12 as shown below:
```xml
<?xml version="1.0"?>
<robot name="turtlebot3_burger_sim" xmlns:xacro="http://ros.org/wiki/xacro">
    ...
    <gazebo reference="base_scan">
        ...
        <sensor type="ray" name="lds_lfcd_sensor">
            ...
            <ray>
                ...
                <range>
                    <min>0.08</min>
                    <max>3.5</max>
                    <resolution>0.015</resolution>
                </range>
                ...
            </ray>
            ...
        </sensor>
    </gazebo>
</robot>
```

## Making plots
We have creates some scripts to make the plots used in the report. To make them the the following:

1. Change `False` to `True` in `walle_drive.py`:
```python
if False: # Set to True for constantly printing the velocity and collision count
    data = {
        'current_lin_vel': np.round(bot.current_lin_vel, 2),
        'lin_avg': np.round(bot.speed_average, 3),
        'collisions': np.round(bot.collisions, 3)#,
        # Used to get positional data for each data point. Used to make plots.
        #'pos' : {
        #    'x': np.round(bot.current_pos[0], 4),
        #    'y': np.round(bot.current_pos[1], 4)
        #}
    }
```
2. Run the script as described earlier, while piping the output to a text file:
```
rosrun walle walle_drive.py | tee data/logs/<name>.txt
```
3. Convert the text file to JSON format with `data_parse.py`
```
python3 data/data_parse.py data/logs/<name>.txt
```
4. Use the examples in the Jupyter notebook `data_processing_iterations.ipynb` to see how the plots are made.

## Maintainers
Aleksander N. Nikolajsen; aleknik00@gmail.com  
Mikkel Heinrich;          mikkelheinrich@gmail.com  
Tobias Frejo Rasmussen;   tobias@frejo.dk  
