#!/usr/bin/python
from __future__ import print_function

from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry

import rospy
import numpy as np

CENTER = 0
LEFT   = 1
RIGHT  = 2

LINEAR_VELOCITY  = 0.3
ANGULAR_VELOCITY = 1.5

GET_TB3_DIRECTION = 0
TB3_DRIVE_FORWARD = 1
TB3_RIGHT_TURN    = 2
TB3_LEFT_TURN     = 3

class Turtlebot3Drive():

    cmd_vel_pub_:rospy.Publisher = None
    laser_scan_sub_:rospy.Subscriber = None
    odom_sub_:rospy.Subscriber = None

    escape_range_ = 0.0
    check_forward_dist_ = 0.0
    check_side_dist_ = 0.0

    scan_data_ = [0.0, 0.0, 0.0]

    tb3_pose_ = 0.0
    prev_tb3_pose_ = 0.0

    state_num = 0

    is_colided = False
    collisions = 0
    speed_updates = 0
    speed_accumulated = 0.0
    speed_average = 0.0

    current_lin_vel = 0
    current_pos = (0, 0)

    def __init__(self):

        # initialize ROS parameter
        cmd_vel_topic_name = rospy.get_param("cmd_vel_topic_name", "cmd_vel")

        # initialize variables
        self.escape_range_ = 30.0 * (np.pi / 180.0)
        self.check_forward_dist_ = 0.35
        self.check_side_dist_ = 0.3

        self.tb3_pose_ = 0.0
        self.prev_tb3_pose_ = 0.0

        # initialize publishers
        self.cmd_vel_pub_ = rospy.Publisher(cmd_vel_topic_name, Twist, queue_size=10)

        # initialize subscribers
        self.laser_scan_sub_ = rospy.Subscriber("scan", LaserScan, self.laserScanMsgCallBack, queue_size=10)
        self.odom_sub_ = rospy.Subscriber("odom", Odometry, self.odomMessageCallback, queue_size=10)
    
    def odomMessageCallback(self, data:Odometry):
        siny = 2.0 * (data.pose.pose.orientation.w * data.pose.pose.orientation.z + data.pose.pose.orientation.x * data.pose.pose.orientation.y)
        cosy = 1.0 - 2.0 * (data.pose.pose.orientation.y * data.pose.pose.orientation.y + data.pose.pose.orientation.z * data.pose.pose.orientation.z)
        
        self.tb3_pose_ = np.arctan2(siny, cosy)
        self.current_pos = (data.pose.pose.position.x, data.pose.pose.position.y)

    def laserScanMsgCallBack(self, data:LaserScan):
        scan_angle = [0, 30, 330]

        for i in range(3):
            if np.isinf(data.ranges[i]):
                self.scan_data_[i] = data.range_max
            else:
                self.scan_data_[i] = data.ranges[scan_angle[i]]
        
        if self.scan_data_[CENTER] < 0.1 or min(self.scan_data_[1:]) < 0.14:
            if not self.is_colided:
                #rospy.logwarn("Wall-E has collided")
                self.collisions += 1
                self.is_colided = True
        elif self.is_colided:
            self.is_colided = False
            #rospy.logwarn("Wall-E is free!")

    def updatecommandVelocity(self, lin_vel: float, ang_vel: float):
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_vel
        cmd_vel.angular.z = ang_vel

        self.speed_accumulated += lin_vel
        self.speed_updates += 1
        self.speed_average = self.speed_average + (lin_vel-self.speed_average)/self.speed_updates   # Won't overflow

        self.current_lin_vel = lin_vel

        #rospy.loginfo(cmd_vel)
        self.cmd_vel_pub_.publish(cmd_vel)

    def controlLoop(self):
        #print(self.state_num)
        if self.state_num == GET_TB3_DIRECTION:
            #print(f"Scan: {self.scan_data_}, Fwddist: {self.check_forward_dist_}")
            if self.scan_data_[CENTER] > self.check_forward_dist_:

                if self.scan_data_[LEFT] < self.check_side_dist_:
                    self.prev_tb3_pose_ = self.tb3_pose_
                    self.state_num = TB3_RIGHT_TURN

                elif self.scan_data_[RIGHT] < self.check_side_dist_:
                    self.prev_tb3_pose_ = self.tb3_pose_
                    self.state_num = TB3_LEFT_TURN

                else:
                    self.state_num = TB3_DRIVE_FORWARD
            
            if self.scan_data_[CENTER] < self.check_forward_dist_:
                self.prev_tb3_pose_ = self.tb3_pose_
                self.state_num = TB3_RIGHT_TURN
        
        elif self.state_num == TB3_DRIVE_FORWARD:
            self.updatecommandVelocity(LINEAR_VELOCITY, 0.0)
            self.state_num = GET_TB3_DIRECTION

        elif self.state_num == TB3_RIGHT_TURN:
            if np.abs(self.prev_tb3_pose_ - self.tb3_pose_) >= self.escape_range_:
                self.state_num = GET_TB3_DIRECTION
            else:
                self.updatecommandVelocity(0.0, -1 * ANGULAR_VELOCITY)
            
        elif self.state_num == TB3_LEFT_TURN:
            if np.abs(self.prev_tb3_pose_ - self.tb3_pose_) >= self.escape_range_:
                self.state_num = GET_TB3_DIRECTION
            else:
                self.updatecommandVelocity(0.0, ANGULAR_VELOCITY)
            
        else:
            self.state_num = GET_TB3_DIRECTION


if __name__ == "__main__":
    rospy.init_node("turtlebot3_pydrive")

    bot = Turtlebot3Drive()

    while not rospy.is_shutdown():
        rate = rospy.Rate(100)
        bot.controlLoop()
        #rospy.loginfo(f"Average linear velocity: {bot.speed_average}")
        #rospy.loginfo(f"collisions: {bot.collisions}")
        data = {
                'current_lin_vel': bot.current_lin_vel,
                'lin_avg': bot.speed_average,
                'collisions': bot.collisions,
                'pos' : {
                    'x': bot.current_pos[0],
                    'y': bot.current_pos[1]
                }
            }
        rospy.loginfo(data)

        rate.sleep()
        #rospy.spin()
